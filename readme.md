This is a nodejs/react application. We use mysql as database

We have dockerized our application. You can find Dockerfiles for both frontend and backend in the repo.

### How to run the app on local environment

You can clone our repo and run the app locally using docker itself or kubernetes cluster.

In case of docker, we have provided docker-compose.yml files in the repo. You just need to clone the repo, build the images using Dokcerfiles and then run 
- docker compose up

In case of kubernetes cluster, you will first need to have kubernetes cluster setup. For this purpose, we have setup ansible playbooks which with just one command can install kubectl and minikube on the machine and setup the cluster
- ansible-playbook -i inventory playbook-kubernetes.yml

after that, you will need to run these commands:

- export MYSQLDB_ROOT_PASSWORD=\"{{ secrets.MYSQLDB_ROOT_PASSWORD }}\"; export MYSQLDB_DATABASE=\"{{ secrets.MYSQLDB_DATABASE }}\"; envsubst < /tmp/kube-manifests/mysql-deployment.yml | kubectl apply -f -

- export MYSQLDB_ROOT_PASSWORD=\"{{ secrets.MYSQLDB_ROOT_PASSWORD }}\"; export MYSQLDB_DATABASE=\"{{ secrets.MYSQLDB_DATABASE }}\"; export TAG_COMMIT_BACKEND=\"{{ secrets.TAG_COMMIT_BACKEND }}\"; envsubst < /tmp/kube-manifests/api-deployment.yml | kubectl apply -f -

- export TAG_COMMIT_FRONTEND=\"{{ secrets.TAG_COMMIT_FRONTEND }}\"; envsubst < /tmp/kube-manifests/ui-deployment.yml | kubectl apply -f -

- nohup kubectl port-forward --kubeconfig=/home/mahammadaghdevops/.kube/config --address=0.0.0.0 service/app-ui-service 443:443 &

- nohup kubectl port-forward --kubeconfig=/home/mahammadaghdevops/.kube/config --address=0.0.0.0 service/app-api-service 8080:8080 &

and now application is ready and you can access it on https://localhost:443/


### Below is an explanation of the key sections of .gitlab-ci.yml file:

stages:
  - build
  - test
  - deploy
This defines the stages of the CI/CD pipeline: build, test, and deploy.

include:
  - template: SAST.gitlab-ci.yml
This includes an external template (SAST.gitlab-ci.yml) for Static Application Security Testing (SAST).

variables:
  Define Docker image tags and other variables.
Variables section defines variables used throughout the pipeline, such as image tags, Docker registry credentials, etc.

backend:
  Backend build job configuration.
This defines a job named backend in the build stage for building the backend Docker image.

frontend:
  Frontend build job configuration.
This defines a job named frontend in the build stage for building the frontend Docker image.

test:
  Test job configuration.
This defines a job named test in the test stage. It involves deploying the application to a test server, running tests, and collecting artifacts (e.g., test reports).

deploy:
  Deploy job configuration.
This defines a job named deploy in the deploy stage. It involves deploying the application to a production server.

when: manual
This specifies that the deploy job should be triggered manually, i.e., it won't run automatically but requires manual intervention.

### Explanation of Key Sections of kube-deploy.yml

This playbook automates the deployment of a multi-component application to a production server.
It involves deploying a MySQL database, backend API, and frontend UI, along with setting up port forwarding for remote access.
Key Steps:

Copy Deployment Files:

Copies Kubernetes deployment manifests from /home/mahammadaghdevops/kube-manifeststo the target server's /tmpdirectory.
Load Secrets:

Securely imports sensitive variables like database passwords and container image tags from a separate secrets.ymlfile.
Deploy MySQL Database:

Applies the MySQL deployment manifest, setting required environment variables for database configuration.
Pauses for 10 seconds to allow the database to initialize.
Deploy Backend API:

Applies the API deployment manifest, setting environment variables for database connection and container image tag.
Pauses for 30 seconds to allow the API to start.
Deploy Frontend UI:

Applies the frontend deployment manifest, setting the container image tag.
Pauses for 30 seconds to allow the frontend to start.
Start Port Forwarding:

Initiates port forwarding for both frontend (port 443) and backend (port 8080) services, enabling remote access.
Debug Port Forwarding:

Prints status messages from the port forwarding commands for debugging purposes.
Display Network Connections:

Runs netstatto display active network connections, aiding in troubleshooting and verification.

### Testing script explanation

This code automates testing for two API endpoints: /api/getand /api/getFromId/:id.
It ensures these endpoints function correctly and return expected responses.
Key Libraries:

Chai: Assertion library for making expectations about test results.
Chai-HTTP: Plugin for Chai to simplify making HTTP requests within tests.
Structure:

Setup:

Imports necessary libraries and the application itself.
Defines a timeout function to prevent tests from hanging indefinitely.
Includes beforeand afterhooks for setup and cleanup tasks (currently empty).
Test Cases:

GET /api/get:
Verifies that it returns a list of posts with a status code of 200.
Asserts that the response body is an array.
GET /api/getFromId/:id:
Tests retrieving a specific post by ID.
Checks for a 200 status code and an array response body.


### Explanation of Kubernetest manifests

These files define how to deploy two components of an application: the backend API and the frontend UI.
They ensure consistent deployment and configuration in a Kubernetes cluster.
Key Components:

API Deployment:

Creates a single replica of the API pod.
Specifies labels for Kubernetes to identify and manage the pod.
Uses the container image specified by the $TAG_COMMIT_BACKENDvariable.
Runs the command node index.jsto start the API server.
Exposes port 8080 within the container.
Sets environment variables for database connection and port configuration.
API Service:

Exposes the API pod to other services within the cluster.
Maps external port 8080 to the target port 8080 on the API pods.
UI Deployment:

Creates a single replica of the UI pod.
Uses the container image specified by the $TAG_COMMIT_FRONTENDvariable.
Exposes port 443 within the container.
UI Service:

Exposes the UI pod to external traffic.
Maps external port 443 to the target port 443 on the UI pods.

These files define the deployment and service for a MySQL database component within the application.
Key Components:

MySQL Deployment:

Creates a single replica of the MySQL pod.
Uses the mysql:latestcontainer image.
Sets environment variables for the root password and database name (using variables for security).
Exposes port 3306 for database connections.
Mounts an emptyDir volume to persist database data across pod restarts.
MySQL Service:

Exposes the MySQL pod to other services within the cluster.
Maps external port 3306 to the target port 3306 on the MySQL pods.
Integration:

The API deployment depends on this database service, as indicated by the DB_HOSTenvironment variable set to mysqldb-service.

### CI/CD variables:

ANSIBLE_VAULT_PASSWORD
API_URL
MYSQLDB_DATABASE
MYSQLDB_ROOT_PASSWORD
SERVER_IP
SERVER_USER
SSH_PRIVATE_KEY
TEST_SERVER_IP
VAR_REGISTRY
VAR_REGISTRY_IMAGE
VAR_REGISTRY_PASSWORD
VAR_REGISTRY_USER