const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index'); // Update the path to your app.js
const expect = chai.expect;

chai.use(chaiHttp);

describe('API Tests', () => {
    // Define the timeout function
    function timeout(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                reject(new Error('Timeout of ' + ms + 'ms exceeded.'));
            }, ms);
        });
    }

    before((done) => {
        // Perform any setup tasks or database seeding before running tests
        done();
    });

    after((done) => {
        // Perform any cleanup tasks after running tests
        done();
    });

    describe('GET /api/get', () => {
        it('should return a list of posts', (done) => {
            chai.request(app).get('/api/get').end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(res).to.have.status(200);
                expect(res.body).to.be.an('array');

                done();
            });
        });
    });



    describe('GET /api/getFromId/:id', () => {
        it('should return a specific post by ID', (done) => {
            const postId = 20; // Replace with a valid post ID

            chai.request(app).get(`/api/getFromId/${postId}`).end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(res).to.have.status(200);
                expect(res.body).to.be.an('array');

                done();
            });
        });
    });

    // Add more test cases for other API routes as needed

    // ...
});